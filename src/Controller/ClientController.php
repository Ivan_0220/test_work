<?php

namespace App\Controller;

use App\Entity\Client;
use App\Entity\Education;
use App\Form\ClientType;
//use http\Env\Request;
use App\Repository\ClientRepository;
use App\Service\Scoring;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;


class ClientController extends AbstractController
{
    private $rowsCount = 5;

    /**
     * @Route("/clients/add", name="add_client")
     */
    public function add(Request $request, Scoring $scoring)
    {
        $client = new Client();

        $result = $this->setForm($client, $request, $scoring);

        if ($result['status'] == 'success') {
            $this->addFlash('success', 'Форма успешно отправлена');
            return $this->redirectToRoute('add_client');
        }

        return $this->render('client/add.html.twig', [
            'form' => $result['form']->createView(),
            'controller_name' => 'ClientController',
            'title' => 'Форма добавления клиента'
        ]);
    }

    /**
     * @Route("/clients/update/{id}", name="update_client")
     */
    public function update(int $id, Request $request, Scoring $scoring, ClientRepository $clients)
    {
        $client = $clients->find($id);

        $result = $this->setForm($client, $request, $scoring);

        if ($result['status'] == 'success') {
            $this->addFlash('success', 'Данные клиента обновлены');
            return $this->redirectToRoute('update_client', ['id' => $id]);
        }

        return $this->render('client/add.html.twig', [
            'form' => $result['form']->createView(),
            'controller_name' => 'ClientController',
            'title' => 'Форма редактирования клиента'
        ]);
    }

    /**
     * @param $client
     * @param Request $request
     * @param Scoring $scoring
     * @return array
     */
    private function setForm($client, Request $request, Scoring $scoring)
    {
        $options = [
            "educationOptions" => $this->getEducations()
        ];

        $form = $this->createForm(ClientType::class, $client, $options);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $client = $form->getData();
            $client->setScoring($scoring->getScoring($client));
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($client);
            $entityManager->flush();
            $status = 'success';
        } else {
            $status = 'error';
        }
        return ['form' => $form, 'status' => $status];
    }

    /**
     * @Route("/clients/{page}", name="clients", requirements={"page"="\d+"})
     */
    public function showClientList(int $page = 1, ClientRepository $clients)
    {
        $latestPosts = $clients->findLatestCustom($page, $this->rowsCount);

        return $this->render('client/list.html.twig', [
            'controller_name' => 'ClientController',
            'paginator' => $latestPosts
        ]);
    }

    /**
     * @Route("/clients/detail/{id}", name="clientdetail")
     */
    public function showClientDetail(int $id, ClientRepository $clients)
    {
        $clientInfo = $clients->findById($id);

        return $this->render('client/detail.html.twig', [
            'controller_name' => 'ClientController',
            'info' => $clientInfo
        ]);
    }

    /**
     * @return array
     */
    private function getEducations()
    {
        $arEducations = [];
        $arEduObj = $this->getDoctrine()->getRepository(Education::class)->findAll();
        foreach ($arEduObj as $eduObj) {
            $arEducations[$eduObj->getName()] = $eduObj;
        }
        return $arEducations;
    }
}