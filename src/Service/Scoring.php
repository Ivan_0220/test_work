<?php

namespace App\Service;

class Scoring
{
    private $phonePoints;
    private $domenPoints;
    private $educationPoints;
    private $agreeStatusPoints;


    public function getScoring($client)
    {
        $this->setParams($client);
        $scoring = $this->phonePoints + $this->domenPoints + $this->educationPoints + $this->agreeStatusPoints;
        return $scoring;
    }

    private function setParams($client)
    {
        $this->setPhonePoints($client->getPhone());
        $this->setDomenPoints($client->getEmail());
        $this->setEducationPoints($client->getEducation());
        $this->setAgreeStatusPoints($client->getAgree());
    }

    public function setPhonePoints($phone)
    {
        $this->phonePoints = 0;
        preg_match('#(?<=\()[\d]{3}(?=\))#', $phone, $matches, PREG_UNMATCHED_AS_NULL);

        if (!empty($matches)) {
            $phoneCode = reset($matches);

            $mts = [901, 902, 904, 908, 910, 911, 912, 913, 914, 915, 916, 917, 918, 919, 950, 978, 980, 981, 982, 983, 984, 985, 986, 987, 988, 989];
            $beeline = [900, 902, 903, 904, 905, 906, 908, 909, 950, 951, 953, 960, 961, 962, 963, 964, 965, 966, 967, 968, 969, 980, 983, 986];
            $megafon = [902, 904, 908, 920, 921, 922, 923, 924, 925, 926, 927, 928, 929, 930, 931, 932, 933, 934, 936, 937, 938, 939, 950, 951, 999];

            if (in_array($phoneCode, $megafon) !== false) {
                $this->phonePoints = 10;
            } elseif (in_array($phoneCode, $beeline) !== false) {
                $this->phonePoints = 5;
            } elseif (in_array($phoneCode, $mts) !== false) {
                $this->phonePoints = 3;
            } else {
                $this->phonePoints = 1;
            }
        }
    }

    public function getPhonePoints()
    {
        return $this->phonePoints;
    }

    public function setDomenPoints($email)
    {
        $this->domenPoints = 0;

        preg_match('#(?<=\@).+(?=\.)#', $email, $matches, PREG_UNMATCHED_AS_NULL);

        if (!empty($matches)) {
            $emailDomen = reset($matches);
            switch ($emailDomen) {
                case "gmail":
                    $this->domenPoints = 10;
                    break;
                case "yandex":
                    $this->domenPoints = 8;
                    break;
                case "mail":
                    $this->domenPoints = 6;
                    break;
                default:
                    $this->domenPoints = 3;
            }
        }
    }

    public function getDomenPoints()
    {
        return $this->domenPoints;
    }

    public function setEducationPoints($education)
    {
        switch ($education->getCode()){
            case "higher":
                $this->educationPoints = 15;
                break;
            case "special":
                $this->educationPoints = 10;
                break;
            case "secondary":
                $this->educationPoints = 5;
                break;
        }
    }

    public function getEducationPoints()
    {
        return $this->educationPoints;
    }

    public function setAgreeStatusPoints($isAgree)
    {
        if ($isAgree) {
            $this->agreeStatusPoints = 4;
        } else {
            $this->agreeStatusPoints = 0;
        }
    }

    public function getAgreeStatusPoints()
    {
        return $this->agreeStatusPoints;
    }
}