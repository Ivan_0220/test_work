<?php

namespace App\Repository;

use App\Entity\Client;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Client|null find($id, $lockMode = null, $lockVersion = null)
 * @method Client|null findOneBy(array $criteria, array $orderBy = null)
 * @method Client[]    findAll()
 * @method Client[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClientRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Client::class);
    }

    public function findByID(int $id)
    {
        $sql = 'SELECT * FROM client as a 
        LEFT JOIN (SELECT name as education, id as education_id FROM education) as b 
        ON a.education_id = b.education_id
        WHERE a.id = ' . $id;
        $conn = $this->getEntityManager()->getConnection();
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();

        return reset($result);
    }

    public function findLatestCustom(int $page = 1, int $limit = 10)
    {
        $sql1 = 'SELECT COUNT(*) FROM client';

        $sql2 = 'SELECT * FROM client as a 
        LEFT JOIN (SELECT name as education, id as education_id FROM education) as b 
        ON a.education_id = b.education_id 
        ORDER BY a.id ASC
        LIMIT ' . ($page - 1) * $limit . ', ' . $limit;

        $conn = $this->getEntityManager()->getConnection();
        $stmt = $conn->prepare($sql1);
        $stmt->execute();
        $result = $stmt->fetchAll();

        $countClients = reset($result)['COUNT(*)'];

        if ($countClients > 0) {
            $conn = $this->getEntityManager()->getConnection();
            $stmt = $conn->prepare($sql2);
            $stmt->execute();

            $lastPage = ceil($countClients / $limit);

            $paginationParams = [
                'currentPage' => $page,
                'hasPreviousPage' => $page > 1,
                'hasNextPage' => $page < $lastPage,
                'hasToPaginate' => $lastPage > 1,
                'results' => $stmt->fetchAll(),
                'lastPage' => $lastPage,
                'nextPage' => min($lastPage, $page + 1),
                'previousPage' => max(1, $page - 1)
            ];
        } else {
            $paginationParams = [
                'hasToPaginate' => false,
                'results' => [],
            ];
        }

        return $paginationParams;
    }
}