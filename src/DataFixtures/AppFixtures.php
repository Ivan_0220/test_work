<?php

namespace App\DataFixtures;

use App\Entity\Client;
use App\Entity\Education;
use App\Service\Scoring;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    const REFERENCE = "education";
    private $scoring;
    private $manager;

    public function __construct(Scoring $scoring)
    {
        $this->scoring = $scoring;
    }

    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;
        $this->loadEducations();
        $this->loadClients();
    }

    private function loadEducations()
    {

        foreach ($this->getEducations() as $key => [$code, $name]) {
            $edu = new Education();
            $edu->setCode($code);
            $edu->setName($name);
            $this->manager->persist($edu);
            $this->addReference(self::REFERENCE . '_' . $key, $edu);
        }

        $this->manager->flush();
    }

    private function getEducations()
    {
        return [
            ['secondary', 'Среднее образование'],
            ['special', 'Специальное образование'],
            ['higher', 'Высшее образование']
        ];
    }

    private function loadClients()
    {
        foreach ($this->getClients() as [$name, $surname, $phone, $email, $edu, $agree]) {
            $client = new Client();
            $client->setName($name);
            $client->setSurname($surname);
            $client->setPhone($phone);
            $client->setEmail($email);
            $client->setEducation($this->getReference(self::REFERENCE . '_' . $edu));
            $client->setAgree($agree);
            $client->setScoring($this->scoring->getScoring($client));
            $this->manager->persist($client);
        }

        $this->manager->flush();
    }

    private function getClients()
    {
        return [
            ['Пётр', 'Бекетов', '+7(901)111-22-33', 'test@mail.ru', '1', true],
            ['Иван', 'Ребров', '+7(999)111-22-33', 'test@mail.ru', '2', true],
            ['Максим', 'Перфильев', '+7(999)111-22-33', 'test@mail.ru', '0', true],
            ['Иван', 'Москвитин', '+7(999)111-22-33', 'test@mail.ru', '1', true],
            ['Михаил', 'Стадухин', '+7(999)111-22-33', 'test@mail.ru', '0', true],
            ['Курбат', 'Иванов', '+7(999)111-22-33', 'test@mail.ru', '2', true],
            ['Василий', 'Поярков', '+7(999)111-22-33', 'test@mail.ru', '2', true],
            ['Семён', 'Дежнёв', '+7(999)111-22-33', 'test@mail.ru', '1', false],
            ['Федот', 'Попов', '+7(999)111-22-33', 'test@mail.ru', '1', false],
        ];
    }
}