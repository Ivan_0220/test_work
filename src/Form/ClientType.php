<?php

namespace App\Form;

use App\Entity\Client;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TelType;


class ClientType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class, ['label' => 'Имя']);
        $builder->add('surname', TextType::class, ['label' => 'Фамиилия']);
        $builder->add('phone', TelType::class, [
            'attr' => [
                'pattern' => '\+7\([\d]{3}\)[\d]{3}-[\d]{2}-[\d]{2}',
                'title' => '+7(___)___-__-__'
            ],
            'label' => 'Номер телефона'
        ]);
        $builder->add('email', EmailType::class, ['label' => 'E-mail']);
        $builder->add('education', ChoiceType::class, [
            'choices' => $options["educationOptions"]
        ]);
        $builder->add('agree', CheckboxType::class, [
            'required' => false,
            'label' => 'Я даю согласие на обработку моих личных данных'
        ]);
        $builder->add('save', SubmitType::class, [
            'label' => 'Сохранить'
        ]);
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Client::class,
            'educationOptions' => false,
        ]);
    }
}